from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.migrate import Migrate
from flask.ext.cors import CORS
import config
import jinja2

app=Flask(__name__,static_folder='../static', static_url_path="")
app.jinja_loader = jinja2.FileSystemLoader('templates/src')
manager=Manager(app)
app.secret_key="reality is broken"
app.config.from_object("config")
app.config['CORS_HEADERS']='Content-Type'
cors=CORS(app)
db=SQLAlchemy(app)
migrate=Migrate(app,db)
from app import models,views
